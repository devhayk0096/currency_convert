function convert() {
    let form = $('#currency-convert-form'),
        url = form.attr('action'),
        method = form.attr('method'),
        data = form.serialize();

    $.ajax({
        method: method,
        url: url,
        data: data,
        beforeSend: () => {
            $('#form-errors').html('');
        },
        success: response => {
            $('#result').html(response.data);
        },
        error: response => {
            if (response.status === 422) {
                let errors = response.responseJSON.errors,
                    errorsHtml = '';

                $.each(errors, (key, value) => {
                    errorsHtml += '<div class="text-danger">' + value[0] + '</div>';
                });

                $('#form-errors').html(errorsHtml);
            }
        }
    });
}

$().ready( () => {
    $('#convert-btn').on('click', function (){
        convert();
    });

    $('#currency-convert-form').on('keyup keypress', function(e) {
        let keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            convert();
        }
    });
});
