<?php

namespace App\Services;

class BaseService implements ServiceInterface
{
    protected array $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function run()
    {

    }
}
