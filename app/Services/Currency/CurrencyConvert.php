<?php

namespace App\Services\Currency;

use App\Services\BaseService;

class CurrencyConvert extends BaseService
{
    public function convert(): string
    {
        $amount = isset($this->data['amount']) ? $this->data['amount'] : 1;
        $apikey = 'd1ded944220ca6b0c442';

        $from_Currency = urlencode($this->data['from_currency']);
        $to_Currency = urlencode($this->data['to_currency']);
        $query = "{$from_Currency}_{$to_Currency}";

        $json = file_get_contents("http://free.currencyconverterapi.com/api/v5/convert?q={$query}&compact=y&apiKey={$apikey}");
        $obj = json_decode($json, true);

        $val = $obj["$query"];
        $total = $val['val'] * $amount;
        $formatValue = number_format($total, 3, '.', '');
        return "$amount $from_Currency = $formatValue $to_Currency";
    }

    public function run()
    {
        return $this->convert();
    }
}
