<?php

namespace App\Services\Currency;

use App\Services\BaseService;

class CurrencyCodes extends BaseService
{
    /**
     * Return currency codes array
     *
     * @return array
     */
    public function getCurrencyCodesFromArray(): array
    {
        $currenciesArray = (new CurrencyList())->run();
        $currencyCodes = [];

        if (count($currenciesArray) > 0) {
            $currencyCodeKey = 'CharCode';

            foreach ($currenciesArray as $currencyArray) {
                if (array_key_exists($currencyCodeKey, $currencyArray)) {
                    $currencyCodes[] = $currencyArray[$currencyCodeKey];
                }
            }
        }

        return $currencyCodes;
    }

    public function run()
    {
        return $this->getCurrencyCodesFromArray();
    }
}
