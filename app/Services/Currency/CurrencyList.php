<?php

namespace App\Services\Currency;

use App\Services\BaseService;

class CurrencyList extends BaseService
{
    private string $defaultXMLUrl = 'http://www.cbr.ru/scripts/XML_daily.asp';

    private string $defaultCurrencyKey = 'Valute';

    public function __construct($data = [])
    {
        parent::__construct($data);

        $this->data['xml_url'] = array_key_exists('xml_url', $data) && !empty($data['xml_url'])
            ? $data['xml_url']
            : $this->defaultXMLUrl;

        $this->data['currencyKey'] = array_key_exists('currencyKey', $data) && !empty($data['currencyKey'])
            ? $data['currencyKey']
            : $this->defaultCurrencyKey;
    }

    /**
     * Get currencies from xml document
     *
     * @return array|mixed
     */
    public function getCurrencies()
    {
        $xmlString = file_get_contents($this->data['xml_url']);
        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);
        $data = json_decode($json, true);

        $currencyKey = $this->data['currencyKey'];

        return array_key_exists($currencyKey, $data) ? $data[$currencyKey] : [];
    }

    public function run()
    {
        return $this->getCurrencies();
    }
}
