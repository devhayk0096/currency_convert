<?php

namespace App\Http\Controllers;

use App\Http\Requests\Currency\ConvertRequest;
use App\Services\Currency\CurrencyConvert;
use App\Services\Currency\CurrencyCodes;
use Illuminate\Http\JsonResponse;

class CurrenciesController extends Controller
{
    /**
     * Get list of currencies
     */
    public function index()
    {
        $currencyCodes = (new CurrencyCodes())->run();

        return view('currencies.index', compact('currencyCodes'));
    }

    /**
     * Get converted currency data
     *
     * @param ConvertRequest $request
     * @return JsonResponse
     */
    public function convert(ConvertRequest $request): JsonResponse
    {
        $data = (new CurrencyConvert($request->all()))->run();

        return response()->json(['data' => $data]);
    }

}
