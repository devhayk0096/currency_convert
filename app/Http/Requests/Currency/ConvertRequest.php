<?php

namespace App\Http\Requests\Currency;

use App\Services\Currency\CurrencyCodes;
use Illuminate\Foundation\Http\FormRequest;

class ConvertRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'from_currency' => ['required', 'in:'. implode(',', (new CurrencyCodes())->run())],
            'to_currency'   => ['required', 'in:'. implode(',', (new CurrencyCodes())->run())],
            'amount'        => ['nullable', 'numeric']
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'from_currency.in' => 'Invalid currency code in the "From" field',
            'to_currency.in'   => 'Invalid currency code in the "To" field',
        ];
    }
}
