@extends('app')

@section('content')
    <div class="form-group mb-3">
        <form action="{{ route('currency.convert') }}" method="post" id="currency-convert-form">
            @csrf
            <div class="col-md-12">
                <div id="form-errors" class="pb-3"></div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="from">From</label>
                        <select class="custom-select" id="currency-from" name="from_currency">
                            @foreach($currencyCodes as $currencyCode)
                                <option value="{{ $currencyCode }}">{{ $currencyCode }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputPassword4">To</label>
                        <select class="custom-select" id="currency-to" name="to_currency">
                            @foreach($currencyCodes as $currencyCode)
                                <option value="{{ $currencyCode }}">{{ $currencyCode }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control" id="amount" name="amount">
                    </div>
                </div>
                <div class="form-row">
                    <button class="btn btn-sm btn-primary" type="button" id="convert-btn">Convert</button>
                    <div id="result" class="pl-5"></div>
                </div>
            </div>
        </form>
    </div>
@endsection


@section('js')
    <script src="{{ asset('js/currency.js') }}"></script>
@endsection
